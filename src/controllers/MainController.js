import React, { Component } from 'react';
import {
    StyleSheet,
    View
} from 'react-native';
import KSView, {KSContainedView} from '../views/KSView'

export default class MainController extends Component {

    render() {
        return (
            <View style={styles.container}>
                <KSContainedView style={{backgroundColor: 'red', width: 250 }}
                                 height={50}/>
                <KSView myPassedInProp={100}/>
                <KSView myPassedInProp={20}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1
    }
})