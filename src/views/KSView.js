import React, { Component } from 'react';
import {
    TouchableHighlight,
    StyleSheet,
    View
} from 'react-native';

export class KSContainedView extends Component {
    render() {
        return (
            <View style={this.props.style} height={this.props.height}/>
        )
    }
}

export default class KSView extends Component {

    // Mounting

    logColor(method) {
        console.log('\x1b[32m' + this.constructor.name + " : " + method + '\x1b[30m')
    }

    constructor(props) {
        super(props)
        this.logColor(arguments.callee.name)
        this.state = {
            containerHeight: this.props.myPassedInProp
        }
    }

    componentWillMount(){
        this.logColor(arguments.callee.name)
    }

    componentDidMount(){
        this.logColor(arguments.callee.name)
    }

    // Updating
    componentWillReceiveProps(nextProps){
        this.logColor(arguments.callee.name)
    }

    shouldComponentUpdate(nextProps, nextState){
        this.logColor(arguments.callee.name)
        return true
    }

    componentWillUpdate(nextProps, nextState){
        this.logColor(arguments.callee.name)
    }

    componentDidUpdate(prevProps, prevState){
        this.logColor(arguments.callee.name)
    }

    componentWillUnmount(){
        this.logColor(arguments.callee.name)
    }

    generateRandomNumberBetween(lowest, highest) {
        return Math.floor((Math.random() * (highest - lowest)) + lowest);
    }

    onPress() {

        var height = this.generateRandomNumberBetween(10, 50);

        this.setState({
            containerHeight: height
        })
    }

    render() {
        this.logColor(arguments.callee.name)
        const additionalStyle = {
            borderWidth: 2,
            borderColor: 'blue',
            backgroundColor: 'orange'
        }

        return (
            <View style={styles.container}>
                <View style={styles.demoView}>
                    <TouchableHighlight onPress={this.onPress.bind(this)}>
                        <View
                            style={[styles.containedView, additionalStyle]}
                            height={this.state.containerHeight}
                        />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        padding: 20,
        paddingTop: 5,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 20,
    },
    demoView: {
        flex: 1,
        opacity: 1,
        backgroundColor: 'white',
        borderWidth: 5,
        borderRadius: 15,
        borderColor: 'green',
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center', // Aligns items cross directionally
        justifyContent: 'center', // Aligns items in the main direction
    },
    containedView: {
        backgroundColor: 'red',
        height: 50,
        width: 50,

        shadowColor: 'black', // ios only
        shadowOffset: {width: 5, height: 5}, // ios only
        shadowOpacity: 0.5, // ios only
        shadowRadius: 1, // ios only

        transform: [
            // {perspective: 100},
            // {rotate: '45deg'},
            // {rotateX: '45deg'},
            // {rotateY: '45deg'},
            // {rotateZ: '45deg'},
            // {translateX: 50},
            // {translateY: 50},
            // {scale: 2},
            // {scaleX: 3},
            // {scaleY: 4},
            // {skewX: '45deg'},
            // {skewY: '45deg'},
        ]
    }

});